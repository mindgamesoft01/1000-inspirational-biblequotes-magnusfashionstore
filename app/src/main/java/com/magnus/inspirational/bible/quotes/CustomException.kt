package com.magnus.inspirational.bible.quotes

class CustomException(message: String) : Exception(message)
